package com.daioware.net.http.copier;

import com.daioware.commons.ProgressNotifier;
import com.daioware.stream.Printer;

public class WebSiteCopierProgressNotifier extends ProgressNotifier{

	private Printer printer;
	
	public WebSiteCopierProgressNotifier(float currentProgress, Float totalProgress,Printer printer) {
		super(currentProgress, totalProgress);
	}
	
	public WebSiteCopierProgressNotifier(float totalProgress,Printer printer) {
		super(totalProgress);
		setPrinter(printer);
	}
	
	public Printer getPrinter() {
		return printer;
	}
	
	public void setPrinter(Printer printer) {
		this.printer = printer;
	}

	public WebSiteCopierProgressNotifier(Float totalProgress) {
		super(totalProgress);
	}

}
