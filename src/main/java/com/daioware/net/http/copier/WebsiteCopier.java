package com.daioware.net.http.copier;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

import com.daioware.commons.string.Regex;
import com.daioware.net.http.HttpResponse;
import com.daioware.net.http.HttpUtil;
import com.daioware.net.http.client.FileHandler;
import com.daioware.net.http.client.HandlingException;
import com.daioware.net.http.client.HttpSender;
import com.daioware.net.http.items.HttpHeader;
import com.daioware.net.http.request.HttpGetRequest;
import com.daioware.net.http.request.HttpRequest;
import com.daioware.stream.Printer;

public class WebsiteCopier {

	public static final String DEFAULT_REGEX=".*";
	
	private String url;
	private File outputFolder;
	private Printer printer;
	private int downloadSpeedInBytes;
	private WebSiteCopierProgressNotifier progressNotifier;
	private Regex regex;
	
	public WebsiteCopier(String url,File outputFolder) {
		this(url,outputFolder,Printer.defaultPrinter,1024,new WebSiteCopierProgressNotifier(0,Printer.defaultPrinter));
	}

	public WebsiteCopier(String url, File outputFolder, Printer printer, int downloadSpeed,WebSiteCopierProgressNotifier progressNotifier) {
		setUrl(url);
		setOutputFolder(outputFolder);
		setPrinter(printer);
		setDownloadSpeedInBytes(downloadSpeed);
		setProgressNotifier(progressNotifier);
	}

	public Regex getRegex() {
		return regex;
	}

	public void setRegex(Regex regex) {
		this.regex = regex;
	}

	public WebSiteCopierProgressNotifier getProgressNotifier() {
		return progressNotifier;
	}

	public void setProgressNotifier(WebSiteCopierProgressNotifier progressNotifier) {
		this.progressNotifier = progressNotifier;
	}

	public Printer getPrinter() {
		return printer;
	}

	public int getDownloadSpeedInBytes() {
		return downloadSpeedInBytes;
	}

	public void setPrinter(Printer printer) {
		this.printer = printer;
	}

	public void setDownloadSpeedInBytes(int downloadSpeed) {
		this.downloadSpeedInBytes = downloadSpeed;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public File getOutputFolder() {
		return outputFolder;
	}

	public void setOutputFolder(File outputFolder) {
		this.outputFolder = outputFolder;
	}
	
	public void copy() throws UnknownHostException, IOException, HandlingException {
		HttpRequest request=new HttpGetRequest(getUrl());
		String href;
		File file,outputFolderForNewElement;
		Element doc;
		HttpHeader header;
		Regex regex=getRegex();
		if(regex==null) {
			regex=new Regex(DEFAULT_REGEX);
		}
		HttpResponse resp;
		HttpSender sender=new HttpSender(request);
		request.addHeader("Connection","close");
		sender.setSaveResponseInMemory(false);
		sender.setProgressNotifier(getProgressNotifier());
		sender.setRespHandler(new FileHandler());
		sender.setOutputFolder(getOutputFolder());
		printer.println("Downloading "+getUrl());
		sender.setDownloadSpeedInBytes(getDownloadSpeedInBytes());
		resp=sender.send();
		file=resp.getBodyAsFile();
		header=resp.getHeaders().get("Content-Type");
		if(header!=null && header.getValue().contains("text/html")) {
			doc=Jsoup.parse(new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())))).body();
			for(Element e:doc.getElementsByTag("a")) {
				href=e.attr("href");
				if(regex.matches(href)) {
					outputFolderForNewElement=getOutputFolder(outputFolder.getAbsolutePath()+File.separator+href);
					if(!outputFolderForNewElement.mkdirs() && !outputFolderForNewElement.exists()) {
						throw new IOException("Could not create dir "+outputFolderForNewElement);
					}
					new WebsiteCopier(getUrl(getUrlWithSlash(),href), outputFolderForNewElement).copy();
				}
			}
		}	
	}
	
	protected File getOutputFolder(String file) {
		if(!file.endsWith("/")) {//Means it is a file, not a web folder
			return new File(file).getParentFile();
		}
		else {
			return new File(file);
		}
	}
	
	protected String getUrlWithSlash() {
		String url=getUrl();
		return url.charAt(url.length()-1)!='/'?url+"/":url;
	}
	
	protected static String getUrl(String parentUrl,String childUrl) {
		if(childUrl.startsWith("http://") || childUrl.startsWith("https://")) {
			return childUrl;
		}
		else {
			return parentUrl+childUrl;
		}
	}
	public static void main(String[] args) throws Exception{
		Regex regex=new Regex(".*(.mkv|.mp4|.avi|.html|./)"); 
		String show="The Big Bang Theory";
		String season="S07";
		String url="http://dl.dlb3d.xyz/S/The.Big.Bang.Theory/S07/480p.x264.BluRay/";
		url=HttpUtil.encodePath(url);
		File outputFolder=new File("C:\\Users\\Diego Olvera\\Videos\\TV Series\\"+show+File.separator+season+File.separator);
		outputFolder.mkdirs();
		WebsiteCopier webSiteCopier=new WebsiteCopier(url, outputFolder);
		webSiteCopier.setRegex(regex);
		webSiteCopier.copy();
	}
	
}
